interface QueriesListProps {
  queries: { query: string; id: number }[];
  onQuerySelect: (query: string) => void;
}

export default function QueriesList({
  queries,
  onQuerySelect,
}: QueriesListProps) {
  return (
    <div className="query-list">
      {queries.map(({ id, query }) => (
        <div
          key={id}
          onClick={() => onQuerySelect(query)}
          className="query-list-item"
        >
          {query}
        </div>
      ))}
    </div>
  );
}
