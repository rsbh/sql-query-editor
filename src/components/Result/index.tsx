import { ResultItem } from "../../helpers/queries";
import {
  transformQueryDataToTableHeaders,
  transformQueryDataToTableRows,
} from "../../helpers/table";

interface ResultProps {
  data: ResultItem[];
}

export default function Result({ data }: ResultProps) {
  const headers = transformQueryDataToTableHeaders(data);
  const rows = transformQueryDataToTableRows(data);
  return (
    <div className="result-table">
      <table>
        <thead>
          <tr>
            {headers.map((header) => (
              <th key={header}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, rowIndex) => (
            <tr key={rowIndex}>
              {row.map((cell, cellIndex) => (
                <td key={`${rowIndex}-${cellIndex}`}>{cell}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
