import Editor from "@monaco-editor/react";
import { useEffect, useState } from "react";

interface SQLEditorProps {
  query: string;
  onSubmit: (query: string) => void;
}

export default function SQLEditor({ query, onSubmit }: SQLEditorProps) {
  const [queryText, setQueryText] = useState(query);

  function handleEditorChange(value: string = "") {
    setQueryText(value);
  }

  function handleSubmit() {
    onSubmit(queryText);
  }

  useEffect(() => {
    setQueryText(query);
  }, [query]);

  return (
    <div className="sql-editor">
      <Editor
        height={"80%"}
        language="sql"
        value={queryText}
        onChange={handleEditorChange}
        options={{
          minimap: {
            enabled: false,
          },
          overviewRulerBorder: false,
        }}
      />
      <div className="query-actions">
        <button onClick={handleSubmit}>Run Query</button>
      </div>
    </div>
  );
}
