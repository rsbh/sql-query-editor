import { faker } from "@faker-js/faker";

const queries = [
  `SELECT * FROM users WHERE id = 1`,
  `SELECT 
  employee.id,
  employee.first_name,
  employee.last_name,
  SUM(DATEDIFF("SECOND", call.start_time, call.end_time)) AS call_duration_sum
FROM call
INNER JOIN employee ON call.employee_id = employee.id
GROUP BY
  employee.id,
  employee.first_name,
  employee.last_name
ORDER BY
  employee.id ASC;`,
  `SELECT 
    single_employee.id,
    single_employee.first_name,
    single_employee.last_name,
    single_employee.call_duration_avg,
    single_employee.call_duration_avg - avg_all.call_duration_avg AS avg_difference
FROM
(
    SELECT 
        1 AS join_id,
        employee.id,
        employee.first_name,
        employee.last_name,
        AVG(DATEDIFF("SECOND", call.start_time, call.end_time)) AS call_duration_avg
    FROM call
    INNER JOIN employee ON call.employee_id = employee.id
    GROUP BY
        employee.id,
        employee.first_name,
        employee.last_name
) single_employee
    
INNER JOIN
    
(
    SELECT
        1 AS join_id,
        AVG(DATEDIFF("SECOND", call.start_time, call.end_time)) AS call_duration_avg
    FROM call
) avg_all ON avg_all.join_id = single_employee.join_id;`,
];

const queriesList = Array(20)
  .fill(0)
  .map((_, i) => ({
    id: faker.datatype.number(),
    query: queries[i % queries.length],
    result: Array(100)
      .fill(0)
      .map(() => ({
        id: faker.datatype.uuid(),
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        job_title: faker.name.jobTitle(),
      })),
  }));

export type ResultItem = Record<string, any>;
export type QueryItem = { id: number; query: string };

function getResult(query: string) {
  const result = queriesList.find((item) => item.query === query);
  if (!query || !result) {
    const randomIndex = Math.floor(Math.random() * queriesList.length);
    return queriesList[randomIndex].result;
  }
  return result?.result;
}

export async function getQueryResult(query: string): Promise<ResultItem[]> {
  return getResult(query);
}

export async function getQueriesList(): Promise<QueryItem[]> {
  return queriesList.map(({ query, id }) => ({
    id,
    query,
  }));
}
