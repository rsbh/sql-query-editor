import { ResultItem } from "./queries";

export function transformQueryDataToTableHeaders(
  data: ResultItem[] = []
): string[] {
  return data.length > 0 ? Object.keys(data[0]) : [];
}

export function transformQueryDataToTableRows(
  data: ResultItem[] = []
): string[][] {
  return data.map((item: ResultItem) => {
    return Object.values(item);
  });
}
