import React, { useEffect, useState, Suspense } from "react";
import { getQueriesList, QueryItem } from "../../helpers/queries";

const QueriesList = React.lazy(() => import("../../components/QueriesList"));
const Editor = React.lazy(() => import("../../components/Editor"));

export default function QueryPanel({
  onSubmit,
}: {
  onSubmit: (query: string) => void;
}) {
  const [query, setQuery] = useState("");
  const [queries, setQueries] = useState<QueryItem[]>([]);

  function onQuerySelect(q: string) {
    setQuery(q);
  }

  function onRunQuery(q: string) {
    setQuery(q);
    onSubmit(q);
  }

  useEffect(() => {
    getQueriesList().then(setQueries);
  }, []);

  return (
    <div className="left-panel">
      <Suspense fallback={<div>Loading...</div>}>
        <Editor query={query} onSubmit={onRunQuery} />
      </Suspense>
      <Suspense fallback={<div>Loading...</div>}>
        <QueriesList queries={queries} onQuerySelect={onQuerySelect} />
      </Suspense>
    </div>
  );
}
