import React, { Suspense } from "react";

import QueryPanel from "./containers/QueryPanel";
import { getQueryResult, ResultItem } from "./helpers/queries";

const Result = React.lazy(() => import("./components/Result"));

function App() {
  const [result, setResult] = React.useState<ResultItem[]>([]);

  function runSQLQuery(query: string) {
    getQueryResult(query).then(setResult);
  }

  return (
    <div className="App">
      <QueryPanel onSubmit={runSQLQuery} />
      <div className="right-panel">
        <Suspense fallback={<div>Loading...</div>}>
          <Result data={result} />
        </Suspense>
      </div>
    </div>
  );
}

export default App;
